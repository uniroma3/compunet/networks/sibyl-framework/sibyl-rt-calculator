import math

from ..node_routing_table import NodeRoutingTable


class RiftSpineRoutingTable(NodeRoutingTable):
    __slots__ = ['plane']

    def __init__(self, name, topology_info):
        super().__init__(name, topology_info)

        self.plane = math.ceil(self.node_number / self.redundancy_factor)

    def compute_routing_table(self):
        pod_subnets, not_pod_subnets = self._get_node_subnets(self.node_name, self.pod['leafs_for_pod'],
                                                              count_to_pod=True)
        # One nexthop to the server subnets in the same pod
        for subnet in pod_subnets:
            self.routing_table[subnet] = 1

        self.routing_table["default"] = self.k_top

    def get_routing_table_in_leaf_failure(self, failed_node):
        failed_subnets, _ = self._get_node_subnets(failed_node, 1, count_to_pod=False)

        failure_routing_table = self.routing_table.copy()

        # Lose all the subnets of the failed leaf if it is in my PoD
        (_, failed_pod_num, _, _) = failed_node.split('_')
        if self.node_pod_number == int(failed_pod_num):
            self._delete_subnets(failure_routing_table, failed_subnets)

        return failure_routing_table

    def get_routing_table_in_spine_failure(self, failed_node):
        if self.node_name == failed_node:
            raise Exception("Trying to get routing table of failed node.")

        (_, failed_pod_num, _, failed_node_num) = failed_node.split('_')
        failed_spine_plane = math.ceil(int(failed_node_num) / self.redundancy_factor)

        failure_routing_table = self.routing_table.copy()

        if self.aggregation_layer['number_of_planes'] > 1:
            if self.plane == failed_spine_plane:
                # If R=1 and this spine is not in the PoD of the failed one,
                # there is no connectivity to the server prefixes served by failed spine
                if self.redundancy_factor == 1 and self.node_pod_number != int(failed_pod_num):
                    failed_node_pod_subnets, _ = self._get_node_subnets(failed_node, self.pod['leafs_for_pod'],
                                                                        count_to_pod=True)
                    for subnet in failed_node_pod_subnets:
                        failure_routing_table[subnet] = 'unreachable'

        return failure_routing_table

    def get_routing_table_in_tof_failure(self, failed_node):
        (_, failed_node_plane, _, failed_node_num) = failed_node.split('_')

        failure_routing_table = self.routing_table.copy()

        # If it is a spine of the same plane of the failed ToF it lose one nexthop for each route of the other PoDs
        if self.plane == int(failed_node_plane):
            self._delete_nexthop_for_subnets(failure_routing_table, ['default'])

        return failure_routing_table

    def get_routing_table_in_leaf_spine_link_failure(self, failed_node, failed_interface):
        (failed_node_type, failed_pod_num, _, failed_node_num) = failed_node.split('_')
        if failed_node_type != 'leaf':
            raise Exception("Failed node type in Leaf-Spine link failure must be a leaf. Value: %s" % failed_node)

        connected_spine_num = self._get_spine_by_leaf_iface_number(failed_interface)
        failed_spine_plane = math.ceil(connected_spine_num / self.redundancy_factor)

        failure_routing_table = self.routing_table.copy()
        affected_subnets, _ = self._get_node_subnets(failed_node, 1, count_to_pod=False)

        if self.node_pod_number == int(failed_pod_num) and self.node_number == int(failed_node_num):
            if self.redundancy_factor == 1:
                self._set_nexthop_for_subnets(failure_routing_table, affected_subnets, 'unreachable')
            else:
                self._delete_nexthop_for_subnets(failure_routing_table, affected_subnets)

        elif self.plane == failed_spine_plane:
            if self.redundancy_factor == 1:
                self._set_nexthop_for_subnets(failure_routing_table, affected_subnets, 'unreachable')

        return failure_routing_table

    def get_routing_table_in_spine_tof_link_failure(self, failed_node, failed_interface):
        (failed_node_type, _, _, _) = failed_node.split('_')
        if failed_node_type != 'spine':
            raise Exception("Failed node type in Spine-ToF link failure must be a spine. Value: %s" % failed_node)

        connected_tof_num = self._get_tof_by_spine_iface_number(failed_interface)
        failed_spine_plane = math.ceil(connected_tof_num / self.redundancy_factor)

        failure_routing_table = self.routing_table.copy()

        if self.node_name == failed_node:
            # Affected spine loses a path to the server prefixes of each leaves of other PoDs
            self._delete_nexthop_for_subnets(failure_routing_table, ['default'])
        elif self.redundancy_factor == 1 and self.plane == failed_spine_plane:
            # If R=1, the spines (in the same plane) lose a path to the server prefixes
            failed_pod_subnets, _ = self._get_node_subnets(failed_node, self.pod['leafs_for_pod'], count_to_pod=True)
            self._set_nexthop_for_subnets(failure_routing_table, failed_pod_subnets, self.k_top - 1)

        return failure_routing_table

    def get_routing_table_in_partitioned_fabric(self, failed_node, failed_interfaces):
        (failed_node_type, failed_node_plane, _, _) = failed_node.split('_')
        if failed_node_type != 'tof':
            raise Exception("Failed node type in Partitioned Fabric must be a ToF. Value: %s" % failed_node)

        max_interface_number = max(
            map(lambda x: int("".join([d for d in x if d.isdigit()])), failed_interfaces)
        )
        failed_pod = math.ceil(int(max_interface_number) / self.redundancy_factor)

        failure_routing_table = self.routing_table.copy()

        if self.plane == int(failed_node_plane):
            # If this spine is in the same plane, it loses a multipath to each server prefix of the partitioned ToF
            pod_subnets = self._get_pod_subnets(failed_pod)

            if self.node_pod_number == failed_pod:
                self._delete_nexthop_for_subnets(failure_routing_table, ['default'])
            else:
                self._set_nexthop_for_subnets(failure_routing_table, pod_subnets, self.k_top - 1)

        return failure_routing_table
