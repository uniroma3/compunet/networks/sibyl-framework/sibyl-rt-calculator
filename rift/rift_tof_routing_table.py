import math

from ..node_routing_table import NodeRoutingTable


class RiftTofRoutingTable(NodeRoutingTable):
    __slots__ = ['plane', 'first_spine_in_plane', 'last_spine_in_plane']

    def __init__(self, name, topology_info):
        super().__init__(name, topology_info)

        self.plane = self.node_pod_number
        self.first_spine_in_plane, self.last_spine_in_plane = self._get_spine_indexes_in_plane(self.plane)

    def compute_routing_table(self):
        for subnet in self.server_subnets:
            self.routing_table[subnet] = self.redundancy_factor

    def get_routing_table_in_leaf_failure(self, failed_node):
        failed_subnets, _ = self._get_node_subnets(failed_node, 1, count_to_pod=False)

        failure_routing_table = self.routing_table.copy()

        # Lose all the subnets of the failed leaf
        self._delete_subnets(failure_routing_table, failed_subnets)

        return failure_routing_table

    def get_routing_table_in_spine_failure(self, failed_node):
        (_, _, _, failed_node_num) = failed_node.split('_')
        failed_pod_subnets, _ = self._get_node_subnets(failed_node, self.pod['leafs_for_pod'], count_to_pod=True)

        failure_routing_table = self.routing_table.copy()

        if self.first_spine_in_plane <= int(failed_node_num) <= self.last_spine_in_plane:
            # Failed spine in the same plane of this ToF, remove one next hop for subnets served by the failed spine
            self._delete_nexthop_for_subnets(failure_routing_table, failed_pod_subnets)

        return failure_routing_table

    def get_routing_table_in_tof_failure(self, failed_node):
        if self.node_name == failed_node:
            raise Exception("Trying to get routing table of failed node.")

        # No changes in routing table of ToFs
        return self.routing_table

    def get_routing_table_in_leaf_spine_link_failure(self, failed_node, failed_interface):
        (failed_node_type, _, _, _) = failed_node.split('_')
        if failed_node_type != 'leaf':
            raise Exception("Failed node type in Leaf-Spine link failure must be a leaf. Value: %s" % failed_node)

        failure_routing_table = self.routing_table.copy()

        connected_spine_num = self._get_spine_by_leaf_iface_number(failed_interface)
        failed_spine_plane = math.ceil(connected_spine_num / self.redundancy_factor)

        if self.plane == failed_spine_plane:
            # If R>1, ToFs (of the same plane) lose a multipath link to the server prefix
            # If R=1, ToFs (of the same plane) lose the server prefix
            affected_subnets, _ = self._get_node_subnets(failed_node, 1, count_to_pod=False)
            self._delete_nexthop_for_subnets(failure_routing_table, affected_subnets)

        return failure_routing_table

    def get_routing_table_in_spine_tof_link_failure(self, failed_node, failed_interface):
        (failed_node_type, _, _, failed_node_num) = failed_node.split('_')
        if failed_node_type != 'spine':
            raise Exception("Failed node type in Spine-ToF link failure must be a spine. Value: %s" % failed_node)

        connected_tof_num = self._get_tof_by_spine_iface_number(failed_interface)
        failed_spine_plane = math.ceil(connected_tof_num / self.redundancy_factor)

        failure_routing_table = self.routing_table.copy()
        server_subnets_in_pod, _ = self._get_node_subnets(self.node_name, self.pod['leafs_for_pod'],
                                                          count_to_pod=True)

        if self.node_number == int(failed_node_num) and self.plane == failed_spine_plane:
            # The ToF connected to the affected spine loses a path to the server prefixes of the affected PoD
            self._delete_nexthop_for_subnets(failure_routing_table, server_subnets_in_pod)

        return failure_routing_table

    def get_routing_table_in_partitioned_fabric(self, failed_node, failed_interfaces):
        (failed_node_type, _, _, _) = failed_node.split('_')
        if failed_node_type != 'tof':
            raise Exception("Failed node type in Partitioned Fabric must be a ToF. Value: %s" % failed_node)

        max_interface_number = max(
            map(lambda x: int("".join([d for d in x if d.isdigit()])), failed_interfaces)
        )
        failed_pod = math.ceil(int(max_interface_number) / self.redundancy_factor)

        failure_routing_table = self.routing_table.copy()

        if self.node_name == failed_node:
            # The partitioned ToF loses all the server prefixes of the PoD from which it is partitioned
            failed_pod_subnets = self._get_pod_subnets(failed_pod)
            self._delete_nexthop_for_subnets(failure_routing_table, failed_pod_subnets)

        return failure_routing_table
