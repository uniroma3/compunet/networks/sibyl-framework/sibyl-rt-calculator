from ..node_routing_table import NodeRoutingTable


class RiftLeafRoutingTable(NodeRoutingTable):
    def compute_routing_table(self):
        dc_subnets, not_dc_subnets = self._get_node_subnets(self.node_name, 1, count_to_pod=False)
        for subnet in dc_subnets:
            self.routing_table[subnet] = 1
        self.routing_table['default'] = self.k_leaf

    def get_routing_table_in_leaf_failure(self, failed_node):
        if self.node_name == failed_node:
            raise Exception("Trying to get routing table of failed node.")

        # No changes in routing table of leaves
        return self.routing_table

    def get_routing_table_in_spine_failure(self, failed_node):
        (_, failed_pod_num, _, _) = failed_node.split('_')

        failure_routing_table = self.routing_table.copy()

        if self.node_pod_number == int(failed_pod_num):
            # Leaf on the same PoD of the failed spine

            # Lose one nexthop for the default route
            self._delete_nexthop_for_subnets(failure_routing_table, ['default'])
        else:
            # Leaf not on the same PoD of the failed spine
            if self.redundancy_factor == 1:
                # If R=1 the spine failure causes a partitioned fabric and
                # each leaf of other pods loses a path to the affected pod prefixes
                failed_pod_subnets, _ = self._get_node_subnets(failed_node, self.pod['leafs_for_pod'],
                                                               count_to_pod=True)

                self._set_nexthop_for_subnets(failure_routing_table, failed_pod_subnets, self.k_leaf - 1)

        return failure_routing_table

    def get_routing_table_in_tof_failure(self, failed_node):
        # No changes in routing table of leaves
        return self.routing_table

    def get_routing_table_in_leaf_spine_link_failure(self, failed_node, failed_interface):
        (failed_node_type, failed_pod_num, _, failed_node_num) = failed_node.split('_')
        if failed_node_type != 'leaf':
            raise Exception("Failed node type in Leaf-Spine link failure must be a leaf. Value: %s" % failed_node)

        failure_routing_table = self.routing_table.copy()

        # If I'm the failed leaf, I lose a nexthop for the default route
        if self.node_name == failed_node:
            self._delete_nexthop_for_subnets(failure_routing_table, ['default'])

        # Leaf in same pod. It loses a multipath link for the server prefix
        # If R=1, other pods leaves lose a multipath to the server prefix
        elif self.node_pod_number == int(failed_pod_num) or self.redundancy_factor == 1:

            affected_subnets, _ = self._get_node_subnets(failed_node, 1, count_to_pod=False)

            self._set_nexthop_for_subnets(failure_routing_table, affected_subnets, self.k_leaf - 1)

        return failure_routing_table

    def get_routing_table_in_spine_tof_link_failure(self, failed_node, failed_interface):
        # No changes in routing table of leaves
        return self.routing_table

    def get_routing_table_in_partitioned_fabric(self, failed_node, failed_interfaces):
        # No changes in routing table of leaves
        return self.routing_table
