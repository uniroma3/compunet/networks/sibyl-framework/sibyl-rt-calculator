import importlib


def get_routing_tables(nodes, topology_info):
    return _get_routing_tables_by_scenario(nodes, topology_info)


def get_routing_tables_in_leaf_failure(nodes, topology_info, failed_node):
    return _get_routing_tables_by_scenario(nodes, topology_info, scenario='leaf_failure',
                                           scenario_params={'failed_node': failed_node})


def get_routing_tables_in_spine_failure(nodes, topology_info, failed_node):
    return _get_routing_tables_by_scenario(nodes, topology_info, scenario='spine_failure',
                                           scenario_params={'failed_node': failed_node})


def get_routing_tables_in_tof_failure(nodes, topology_info, failed_node):
    return _get_routing_tables_by_scenario(nodes, topology_info, scenario='tof_failure',
                                           scenario_params={'failed_node': failed_node})


def get_routing_tables_in_leaf_spine_link_failure(nodes, topology_info, failed_node, failed_interface):
    return _get_routing_tables_by_scenario(nodes, topology_info, scenario='leaf_spine_link_failure',
                                           scenario_params={'failed_node': failed_node,
                                                            'failed_interface': failed_interface
                                                            })


def get_routing_tables_in_spine_tof_link_failure(nodes, topology_info, failed_node, failed_interface):
    return _get_routing_tables_by_scenario(nodes, topology_info, scenario='spine_tof_link_failure',
                                           scenario_params={'failed_node': failed_node,
                                                            'failed_interface': failed_interface
                                                            })


def get_routing_tables_in_partitioned_fabric(nodes, topology_info, failed_node, failed_interfaces):
    return _get_routing_tables_by_scenario(nodes, topology_info, scenario='partitioned_fabric',
                                           scenario_params={'failed_node': failed_node,
                                                            'failed_interfaces': failed_interfaces
                                                            })


def _get_routing_tables_by_scenario(nodes, topology_info, scenario=None, scenario_params=None):
    nodes_routing_tables = {}

    protocol_class_name = "".join(map(lambda x: x.capitalize(), topology_info['protocol'].split("_")))

    for node in nodes:
        node_type = node.split('_')[0]

        class_name = "%s%sRoutingTable" % (protocol_class_name, node_type.capitalize())
        module_name = "%s_%s_routing_table" % (topology_info['protocol'], node_type)

        node_routing_table = _class_for_name(".%s.%s" % (topology_info['protocol'], module_name),
                                             class_name
                                             )(node, topology_info)

        if scenario:
            scenario_method = getattr(node_routing_table, 'get_routing_table_in_%s' % scenario)
            nodes_routing_tables[node] = scenario_method(**scenario_params)
        else:
            nodes_routing_tables[node] = node_routing_table.routing_table

    return nodes_routing_tables


def _class_for_name(module_name, class_name):
    m = importlib.import_module(module_name, __package__)
    return getattr(m, class_name)
