import ipaddress
from abc import ABC, abstractmethod


class NodeRoutingTable(ABC):
    __slots__ = ['k_leaf', 'k_top', 'redundancy_factor', 'number_of_pods', 'pod', 'aggregation_layer', 'server_subnets',
                 'routing_table', 'node_name', 'node_pod_number', 'node_number']

    def __init__(self, name, topology_info):
        all_slots = set(NodeRoutingTable.__slots__)
        all_slots.update(self.__slots__)

        self.node_name = name
        (_, self.node_pod_number, _, self.node_number) = name.split('_')
        self.node_pod_number = int(self.node_pod_number)
        self.node_number = int(self.node_number)

        for name, value in topology_info.items():
            if name in all_slots:
                setattr(self, name, value)

        self.routing_table = {}
        self.server_subnets = []

        ipv4_server_subnets = ipaddress.ip_network("200.0.0.0/8").subnets(new_prefix=24)
        total_number_of_server_subnets = self.number_of_pods * self.pod['leafs_for_pod']
        for i in range(0, total_number_of_server_subnets):
            self.server_subnets.append(str(next(ipv4_server_subnets)))

        self.compute_routing_table()

    @abstractmethod
    def compute_routing_table(self):
        raise NotImplemented("Must implement 'get_routing_table' method")

    @abstractmethod
    def get_routing_table_in_leaf_failure(self, failed_node):
        raise NotImplemented("Must implement 'get_routing_table_in_leaf_failure' method")

    @abstractmethod
    def get_routing_table_in_spine_failure(self, failed_node):
        raise NotImplemented("Must implement 'get_routing_table_in_spine_failure' method")

    @abstractmethod
    def get_routing_table_in_tof_failure(self, failed_node):
        raise NotImplemented("Must implement 'get_routing_table_in_tof_failure' method")

    @abstractmethod
    def get_routing_table_in_leaf_spine_link_failure(self, failed_node, failed_interface):
        raise NotImplemented("Must implement 'get_routing_table_in_leaf_spine_link_failure' method")

    @abstractmethod
    def get_routing_table_in_spine_tof_link_failure(self, failed_node, failed_interface):
        raise NotImplemented("Must implement 'get_routing_table_in_spine_tof_link_failure' method")

    @abstractmethod
    def get_routing_table_in_partitioned_fabric(self, failed_node, failed_interfaces):
        raise NotImplemented("Must implement 'get_routing_table_in_partitioned_fabric' method")

    def _get_node_subnets(self, name, count, count_to_pod=True):
        (_, pod, _, number) = name.split('_')

        prev_server_subnets_number = ((int(pod) - 1) * self.pod['leafs_for_pod']) + \
                                     (0 if count_to_pod else (int(number) - 1))

        directly_connected_subnets = self.server_subnets[prev_server_subnets_number:prev_server_subnets_number + count]
        not_directly_connected_subnets = [subnet for subnet in self.server_subnets if
                                          subnet not in directly_connected_subnets]

        return directly_connected_subnets, not_directly_connected_subnets

    def _get_pod_subnets(self, pod_number):
        first_leaf_pod = self.pod['leafs_for_pod'] * (pod_number - 1)
        last_leaf_pod = self.pod['leafs_for_pod'] * pod_number

        return self.server_subnets[first_leaf_pod:last_leaf_pod]

    def _get_spine_indexes_in_plane(self, tof_plane):
        last_spine_in_plane = tof_plane * self.redundancy_factor
        first_spine_in_plane = last_spine_in_plane - (self.redundancy_factor - 1)

        return first_spine_in_plane, last_spine_in_plane

    def _get_spine_by_leaf_iface_number(self, iface):
        interface_num = int("".join([d for d in iface if d.isdigit()]))
        spine_num = interface_num + 1
        if spine_num > self.k_leaf:
            raise Exception("Invalid spine number: %d" % spine_num)

        return spine_num

    def _get_tof_by_spine_iface_number(self, iface):
        interface_num = int("".join([d for d in iface if d.isdigit()]))
        tof_num = (interface_num - self.k_top) + 1
        if tof_num > self.k_top:
            raise Exception("Invalid ToF number: %d" % tof_num)

        return tof_num

    @staticmethod
    def _set_nexthop_for_subnets(routing_table, subnets, n_nexthops):
        for subnet in subnets:
            routing_table[subnet] = n_nexthops

    @staticmethod
    def _delete_nexthop_for_subnets(routing_table, subnets, n_nexthops=1):
        for subnet in subnets:
            routing_table[subnet] -= n_nexthops

            if routing_table[subnet] <= 0:
                del routing_table[subnet]

    @staticmethod
    def _delete_subnets(routing_table, subnets):
        for subnet in subnets:
            del routing_table[subnet]
