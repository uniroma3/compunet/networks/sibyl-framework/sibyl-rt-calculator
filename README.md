# Sibyl RT Calculator
A tool for generating the forwarding tables of fat-tree nodes. It takes as input: 
- A topology: in terms of `k_leaf`, `k_top`, `r`;
- A routing protocol: among `BGP`, `RIFT`, `IS-IS`, `Openfabric`; 
- A type of test: among the one of the Sibyl Methodology;

It returns the routing information for each node of the fat-tree. This information contains the **server farm prefixes**
and the **number of next hops** that are expected to be computed according to the test. 

Sibyl RTCalculator is used for the convergence check in the [Sibyl Framework](https://gitlab.com/uniroma3/compunet/networks/sibyl-framework/sibyl/-/tree/master) (see the Sibyl paper on further deatails on the convergence check). 
